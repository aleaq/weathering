# A weather app created from react
Ah yes, yet another weather app made with react with custom CSS SASS styling. 
As I was teaching myself react I needed a little side project. 
<br>
<br>
I decided to go for a minimilist look based off of a dark theme.

![example of weather app](weather-gif.gif)

Check out the demo here: https://aleaq.gitlab.io/weathering/

## Getting started
If you want to clone the repo you'll need your own api key from [openweather](https://openweathermap.org/appid)

After cloning the repo simply run 
 `yarn` or `npm install`

To start the repo:
 `yarn start` or `npm start`

<br>
---
<br>
Shoutout to [Josh Bader](https://twitter.com/joshuabader?lang=en) an amazing UI Designer who made the weather icons. 
